import Exception from '../base'

export default class NotValidArticles extends Exception {

  constructor(message) {
    super()
    this.name = this.constructor.name
    this.message = message || `Some articles in the list are not valid`
  }
}