import Exception from '../base'

export default class MissingProcessor extends Exception {

  constructor(message) {
    super()
    this.name = this.constructor.name
    this.message = message || `missing processor scheduler of the robot`
  }
}