import MissingProcessor from '../exceptions/services/missing_processor'
import NotValidArticles from '../exceptions/services/not_valid_articles'
import EmptyArticles from '../exceptions/services/empty_articles'

export default class Robot {
    constructor(processor) {
        this._processor_scheduler = processor
    }

    set processor_scheduler(value) {
        this._processor_scheduler = value
    }

    get processor_scheduler () {
        return this._processor_scheduler
    }

    pack(articles) {
        if (!this._processor_scheduler) throw new MissingProcessor()
        if (!this.isValidArticles(articles)) throw new NotValidArticles()
        if (articles.length  === 0) throw new EmptyArticles()

        return this._processor_scheduler.process(articles)
    }

    isValidArticles(articles) {
        return /^\d+$/.test(articles);
    }
} 