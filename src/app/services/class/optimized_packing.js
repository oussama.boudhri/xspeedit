import Config from "./config"

export default class OptimizedPacking {
            
    process(articles) {
        return this.organizeAndPackage(articles)
    }

    organizeAndPackage(articles)
    {   
        let articles_packaged = []
        let pack_count = 0;
        let boxes = []
        
        let bin_remaining_space = new Array(articles.length)
     
        for (let i=0; i< articles.length; i++)
        {

            let j
     
            let minimum_left_in_best_bin = Config.CAPACITY+1, best_bin = 0;
     
            for (j = 0; j < pack_count; j++)
            {
                if (bin_remaining_space[j] >= articles[i] &&
                    bin_remaining_space[j] - articles[i] < minimum_left_in_best_bin)
                {
                    best_bin = j;
                    minimum_left_in_best_bin = bin_remaining_space[j] - articles[i];
                    
                }
            }

            if (minimum_left_in_best_bin == Config.CAPACITY+1)
            {
                bin_remaining_space[pack_count] = Config.CAPACITY - articles[i];
                boxes[pack_count] = []
                boxes[pack_count].push(articles[i])
                pack_count++;
                
            }
            else {
                bin_remaining_space[best_bin] -= articles[i]; 
                boxes[best_bin].push(articles[i])
            }
                   
        }
        
        for (let box of boxes) {
            articles_packaged.push(box.join(''))
        }

        return articles_packaged.join(Config.SEPARATOR);
    }
    
  
}