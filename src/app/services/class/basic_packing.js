import Config from "./config"

export default class BasicPacking {

    process(articles) {
        let packaged_articles = ""
        let capacity = Config.CAPACITY
        for(let i = 0; i < articles.length; i++) {
            if((capacity - articles[i]) < 0) {
                packaged_articles += Config.SEPARATOR
                capacity = Config.CAPACITY
            }
            packaged_articles += articles[i]
            capacity -= articles[i]
        }

        return packaged_articles
    }
}