import Robot from './services/robot'
import BasicPacking from './services/class/basic_packing'
import OptimizedPacking from './services/class/optimized_packing'
import Config from './services/class/config'
import Logger from 'winston'

export default class App {

    constructor() {
        this._robot = new Robot(new BasicPacking())
    }

    getNumberOfBox(boxed_articles) {
        return boxed_articles.split(Config.SEPARATOR).length
    }

    launch() {
        let input = "163841689525773"
        if (process.argv[2]) input = process.argv[2]
        const outputBasic = this._robot.pack(input)
        const numberOfBoxBasic = this.getNumberOfBox(outputBasic)

        this._robot._processor_scheduler = new OptimizedPacking()
        const outputOptimized = this._robot.pack(input)
        const numberOfBoxOptimized = this.getNumberOfBox(outputOptimized)

        Logger.info(`Articles : ${input}`)
        Logger.info(`Robot actuel : ${outputBasic} => ${numberOfBoxBasic} cartons utilisés`)
        Logger.info(`Robot optimisé : ${outputOptimized} => ${numberOfBoxOptimized} cartons utilisés`)
    }
}

let app = new App()
app.launch()

