# XspeedIt

[![pipeline status](https://gitlab.com/oussama.boudhri/xspeedit/badges/master/pipeline.svg)](https://gitlab.com/oussama.boudhri/xspeedit/commits/master)

XspeedIt is an import / export company that has automated its entire packaging chain.
She wants to find an algorithm allowing his robots to optimize the number of boxes used.

The articles to be packed are with variable size, represented by an integer between 1 and 9.
Each carton has a capacity of 10.
For example, a carton may contain a size 3 article, a size 1 article, and a size 6 article.

The chain of articles to be packaged is represented by a series of numbers, representing an article by its size.
After processing by the packaging robot, the chain is separated by "/" representing the items contained in a carton.

## Example
```python
List of articles as input : 163841689525773
Result after packaging articles : 163/8/41/6/8/9/52/5/7/73
```
## Output
```python
Articles      : 163841689525773
Result with Basic robot  : 163/8/41/6/8/9/52/5/7/73 => 10 boxes used
Result with optimized robot : 163/82/46/19/8/55/73/7   => 8  boxes used
```

# Install packages
```
$ yarn
```
# Build project
```
$ gulp
```
# Launch app
```
$ npm run start "163841689525773"
```
# Tests
```js
// Run tests
npm run test
```

## License
MIT © [Oussama Boudhri]
