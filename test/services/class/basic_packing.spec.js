import chai from 'chai'
chai.should()
import BasicPacking from '../../../app/services/class/basic_packing'

describe('Basic packing', () => {
  it(`basic packing processor must return a string containing the result`, (done) => {
    let basic_packing = new BasicPacking()  
    const articles = "23423553643"
    basic_packing.process(articles).should.be.ok
    done()
  })

  it(`basic packing processor must pack correctly the articles`, (done) => {
    let basic_packing = new BasicPacking()
    const input = "163841689525773"
    const expected = "163/8/41/6/8/9/52/5/7/73"
    basic_packing.process(input).should.equal(expected)
    done()
  })

})
 