import chai from 'chai'
chai.should()
import OptimizedPacking from '../../../app/services/class/optimized_packing'

describe('Basic packing', () => {
  it(`optimized packing processor must return a string containing the result`, (done) => {
    let optimized_packing = new OptimizedPacking()  
    const input = "163841689525773"
    optimized_packing.process(input).should.be.ok
    done()
  })

  it(`optimized packing processor must return an optimized result`, (done) => {
    let optimized_packing = new OptimizedPacking()  
    const input = "163841689525773"
    const expected = "163/81/46/82/9/55/73/7"
    optimized_packing.process(input).should.equal(expected)
    done()
  })

})
 