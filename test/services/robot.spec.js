import chai from 'chai'
chai.should()
import Robot from '../../app/services/robot'
import BasicPacking from '../../app/services/class/basic_packing'

describe('Robot', () => {
  it(`should return a string containing the result`, (done) => {
    let robot = new Robot(new BasicPacking())
    const input = "234531534"
    robot.pack(input).should.be.ok
    done()
  })

  it(`should throw exception when no processor scheduler is configured`, (done) => {
    (() => {
      let robot = new Robot()
      robot.pack()
    }).should.throw() 
    done()
  })

  it(`should throw an exception when articles list is empty`, (done) => {
    (() => {
      let robot = new Robot(new BasicPacking())
      robot.pack()
    }).should.throw() 
    done()
  })

  it(`should throw an exception when articles list is not valid`, (done) => {
    (() => {
      let robot = new Robot(new BasicPacking())
      const input = "2345Q1H3B" 
      robot.pack(input)
    }).should.throw() 
    done()
  })
  
})
 